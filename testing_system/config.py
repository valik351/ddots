# encoding: utf-8
# pylint: disable=missing-docstring,invalid-name
"""
DDOTS Testing System default config
===================================
"""

import logging
import os
from pathlib import Path


logging.getLogger('requests').setLevel(logging.WARNING)
# Hint: use local_config.py to enable DEBUG logging, e.g.
## import logging
## logging.getLogger('worker').setLevel(logging.DEBUG)
logging.basicConfig(level=logging.INFO)


class BaseConfig(object):
    # This timeout will be used for all Dots API requests
    # default, 30 seconds
    DOTS_DEFAULT_HTTP_TIMEOUT = 30.0

    # Default timeout to download a test tarball is 10 minutes
    DOTS_PROBLEM_DOWNLOAD_TIMEOUT = 600.0

    DOTS_API_URL = os.getenv('DOTS_API_URL') or 'http://127.0.0.1/dots'
    DOTS_USERNAME = os.getenv('DOTS_USERNAME') or 'ddots'
    DOTS_PASSWORD = os.getenv('DOTS_PASSWORD')

    # Keep-Alive HTTP feature saves time and traffic by avoiding reestablishing TCP
    # connections on every request.
    # However, KHCup DOTS setup has issues with keep-alive connections, so as a
    # workaround we can disable Keep-Alive feature in local_settings.
    DOTS_API_USE_KEEP_ALIVE_CONNECTIONS = True

    # DDOTS will lock checkers to a specific CPU core
    try:
        CPU_ID = int(os.getenv('DDOTS_CPU_ID', 0))
    except ValueError:
        CPU_ID = 0

    BASE_CONTAINER_NAME = os.getenv('DDOTS_BASE_CONTAINER_NAME', 'ddots-%d' % CPU_ID)
    SHARED_VOLUMES = {
            'data': {
                    'volume_name': 'ddots-testing-system-%s-data' % BASE_CONTAINER_NAME,
                    # This directory should be mounted to the Testing-System container, and it will
                    # be automatically mounted to compilers and runners in read-only mode.
                    'mount_point': Path('/', 'data'),
                },
            'sandbox': {
                    'volume_name': 'ddots-testing-system-%s-sandbox' % BASE_CONTAINER_NAME,
                    # This directory should be mounted to the Testing-System container, and it will
                    # be automatically mounted to compilers and runners in read-write 777 mode.
                    # NOTE: tmpfs is the best choice here.
                    'mount_point': Path('/', 'sandbox'),
                },
        }

    TEMP_ROOT = Path('/', 'tmp', 'ddots')
    PROBLEMS_DB_ROOT = TEMP_ROOT / 'problems_db'
    PROBLEMS_DB_MAX_CACHED_PROBLEMS = 1000
    SOLUTIONS_TEMP_ROOT = TEMP_ROOT / 'solutions'

    # Note: you can NOT set it to less than 2
    PREFETCH_SOLUTIONS_COUNT = 2
    PREFETCH_DELAY_BETWEEN_CHECKS = 1.0

    # This is the upper bound for compiler and runner containers. Solution test
    # time limit is set from Problem.xml.
    CONTAINER_MEMORY_HARD_LIMIT = 512 * 1024 * 1024  # i.e. 512 (MB)

    DOCKER_DAEMON_API_URL = 'unix://%s' % os.getenv('DOCKER_SOCK_PATH', '/var/run/docker.sock')

    # C
    DOTS_LANG_ID_GCC = '02'
    DOTS_LANG_ID_GCC17 = '18'

    # C++
    DOTS_LANG_ID_GXX = '03'
    DOTS_LANG_ID_GXX11 = '19'
    DOTS_LANG_ID_GXX14 = '20'
    DOTS_LANG_ID_GXX17 = '30'
    DOTS_LANG_ID_GXX20 = '37'

    # Pascal
    DOTS_LANG_ID_FPC = '04'
    DOTS_LANG_ID_FPC_DELPHI = '39'

    # Other compiled languages
    DOTS_LANG_ID_GO = '16'
    DOTS_LANG_ID_HASKELL = '21'
    DOTS_LANG_ID_NIM = '22'
    DOTS_LANG_ID_RUST = '23'
    DOTS_LANG_ID_OCAML = '33'
    DOTS_LANG_ID_SWIFT = '34'
    DOTS_LANG_ID_DART = '36'

    # .NET
    DOTS_LANG_ID_MONO = '14'
    #DOTS_LANG_ID_MONO_BASIC = '29'  # removed since the image was broken and nobody really uses it
    DOTS_LANG_ID_DOTNET_CSHARP = '35'

    # Java and Java-based languages
    DOTS_LANG_ID_OPENJDK7 = '13'
    DOTS_LANG_ID_ORACLEJDK8 = '17'
    DOTS_LANG_ID_SCALA = '24'
    DOTS_LANG_ID_KOTLIN = '26'

    # Python
    DOTS_LANG_ID_PYTHON2 = '11'
    DOTS_LANG_ID_PYTHON3 = '12'
    DOTS_LANG_ID_PYTHON_MACHINELEARNING = '28'

    # Other interpreted languages
    DOTS_LANG_ID_RUBY = '15'
    DOTS_LANG_ID_PHP = '25'
    DOTS_LANG_ID_BASH = '27'
    DOTS_LANG_ID_JAVASCRIPT = '31'


    DOTS_LANG_ID_TO_COMPILER = {
        DOTS_LANG_ID_GCC: 'gcc',
        DOTS_LANG_ID_GCC17: 'gcc17',
        DOTS_LANG_ID_GXX: 'gxx',
        DOTS_LANG_ID_GXX11: 'gxx11',
        DOTS_LANG_ID_GXX14: 'gxx14',
        DOTS_LANG_ID_GXX17: 'gxx17',
        DOTS_LANG_ID_GXX20: 'gxx20',
        DOTS_LANG_ID_FPC: 'fpc',
        DOTS_LANG_ID_FPC_DELPHI: 'fpc-delphi',

        DOTS_LANG_ID_GO: 'go',
        DOTS_LANG_ID_HASKELL: 'haskell',
        DOTS_LANG_ID_NIM: 'nim',
        DOTS_LANG_ID_RUST: 'rust',
        DOTS_LANG_ID_OCAML: 'ocaml',
        DOTS_LANG_ID_SWIFT: 'swift',
        DOTS_LANG_ID_DART: 'dart',

        DOTS_LANG_ID_MONO: 'mono',
        DOTS_LANG_ID_DOTNET_CSHARP: 'dotnet-csharp',

        DOTS_LANG_ID_OPENJDK7: 'openjdk7',
        DOTS_LANG_ID_ORACLEJDK8: 'oraclejdk8',
        DOTS_LANG_ID_SCALA: 'scala',
        DOTS_LANG_ID_KOTLIN: 'kotlin',

        DOTS_LANG_ID_PYTHON2: 'python2',
        DOTS_LANG_ID_PYTHON3: 'python3',
        DOTS_LANG_ID_PYTHON_MACHINELEARNING: 'python-machinelearning',

        DOTS_LANG_ID_RUBY: 'ruby',
        DOTS_LANG_ID_PHP: 'php',
        DOTS_LANG_ID_BASH: 'bash',
        DOTS_LANG_ID_JAVASCRIPT: 'javascript',
    }

    DOTS_LANG_ID_TO_RUNNER = {
        DOTS_LANG_ID_GCC: 'binary',
        DOTS_LANG_ID_GCC17: 'binary',
        DOTS_LANG_ID_GXX: 'binary',
        DOTS_LANG_ID_GXX11: 'binary',
        DOTS_LANG_ID_GXX14: 'binary',
        DOTS_LANG_ID_GXX17: 'binary',
        DOTS_LANG_ID_GXX20: 'binary',
        DOTS_LANG_ID_FPC: 'binary',
        DOTS_LANG_ID_FPC_DELPHI: 'binary',

        DOTS_LANG_ID_GO: 'binary',
        DOTS_LANG_ID_HASKELL: 'binary',
        DOTS_LANG_ID_NIM: 'binary',
        DOTS_LANG_ID_RUST: 'binary',
        DOTS_LANG_ID_OCAML: 'binary',
        DOTS_LANG_ID_SWIFT: 'swift',
        DOTS_LANG_ID_DART: 'dart',

        DOTS_LANG_ID_MONO: 'mono',
        DOTS_LANG_ID_DOTNET_CSHARP: 'dotnet-csharp',

        DOTS_LANG_ID_OPENJDK7: 'openjdk7',
        DOTS_LANG_ID_ORACLEJDK8: 'oraclejdk8',
        DOTS_LANG_ID_SCALA: 'scala',
        DOTS_LANG_ID_KOTLIN: 'kotlin',

        DOTS_LANG_ID_PYTHON2: 'python2',
        DOTS_LANG_ID_PYTHON3: 'python3',
        DOTS_LANG_ID_PYTHON_MACHINELEARNING: 'python-machinelearning',
        DOTS_LANG_ID_RUBY: 'ruby',
        DOTS_LANG_ID_PHP: 'php',
        DOTS_LANG_ID_BASH: 'bash',
        DOTS_LANG_ID_JAVASCRIPT: 'javascript',
    }

    RUNNER_STACK_HARD_LIMIT = 64 * 1024 * 1024  # i.e. 64 (MB)

    RUNNER_TIME_LIMIT_FACTOR = None  # if it is not set, it will be detected automagically
    RUNNER_TIME_LIMIT_FACTOR_STANDARD_IMAGE = 'frolvlad/ddots-one-second-standard'

    RUNNER_EXTRA_OPTIONS = {
        DOTS_LANG_ID_TO_RUNNER[DOTS_LANG_ID_MONO]: {'time_limit_multiplier': 2.0},
        DOTS_LANG_ID_TO_RUNNER[DOTS_LANG_ID_DOTNET_CSHARP]: {'time_limit_multiplier': 2.0},
        DOTS_LANG_ID_TO_RUNNER[DOTS_LANG_ID_OPENJDK7]: {'time_limit_multiplier': 2.0},
        DOTS_LANG_ID_TO_RUNNER[DOTS_LANG_ID_ORACLEJDK8]: {'time_limit_multiplier': 2.0},
        DOTS_LANG_ID_TO_RUNNER[DOTS_LANG_ID_SCALA]: {'time_limit_multiplier': 2.0},
        DOTS_LANG_ID_TO_RUNNER[DOTS_LANG_ID_KOTLIN]: {'time_limit_multiplier': 2.0},

        DOTS_LANG_ID_TO_RUNNER[DOTS_LANG_ID_PYTHON2]: {'time_limit_multiplier': 5.0},
        DOTS_LANG_ID_TO_RUNNER[DOTS_LANG_ID_PYTHON3]: {'time_limit_multiplier': 5.0},
        DOTS_LANG_ID_TO_RUNNER[DOTS_LANG_ID_PYTHON_MACHINELEARNING]: {'time_limit_multiplier': 5.0},
        DOTS_LANG_ID_TO_RUNNER[DOTS_LANG_ID_RUBY]: {'time_limit_multiplier': 5.0},
        DOTS_LANG_ID_TO_RUNNER[DOTS_LANG_ID_PHP]: {'time_limit_multiplier': 5.0},
        DOTS_LANG_ID_TO_RUNNER[DOTS_LANG_ID_JAVASCRIPT]: {'time_limit_multiplier': 2.0},
    }


class TestingConfig(BaseConfig):
    DOTS_PASSWORD = '!'

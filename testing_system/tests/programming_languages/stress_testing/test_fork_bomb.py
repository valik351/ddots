# encoding: utf-8
from pathlib import Path


def test_fork_bomb(compiler_runner, tmpdir, hello_world_problem):
    solution_path = tmpdir.join('fork_bomb.c')
    solution_path.write(
            '#include <unistd.h>\n'
            'int main() {\n'
            '    while(1) fork();\n'
            '    return 0;\n'
            '}'
        )
    testing_report = compiler_runner(
            'gcc11',
            runner='binary',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(solution_path),
            testing_mode='full'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'FF', '0']

def test_fork_bomb_python(compiler_runner, tmpdir, hello_world_problem):
    """
    NOTE: For some reason, fork-bomb in Python resulted in a new corner case with cgroups.
    """
    solution_path = tmpdir.join('fork_bomb.py')
    solution_path.write(
            'import os, time\n'
            'while 1:\n'
            '    os.fork()\n'
            '    time.sleep(0.1)'
        )
    testing_report = compiler_runner(
            'python3',
            runner='python3',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(solution_path),
            testing_mode='full'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'FF', '0']

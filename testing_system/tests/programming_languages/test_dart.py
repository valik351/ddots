# encoding: utf-8
from pathlib import Path
import pytest

from worker.exceptions import CompilationError


def test_dart_OK(compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.dart')
    OK_solution_path.write(
            "import 'dart:io';\n"
            "void main() async { await File('output.txt').writeAsString('Hello World!'); }"
        )
    testing_report = compiler_runner(
            'dart',
            runner='dart',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']


def test_dart_compiler_CE(compiler, tmpdir, hello_world_problem):
    CE_solution_path = tmpdir.join('CE_solution.dart')
    CE_solution_path.write('if')
    with pytest.raises(CompilationError) as CE:
        compiler(
                'dart',
                problem_path=hello_world_problem['path'],
                solution_source_path=Path(CE_solution_path)
            )
    assert 'solution.source:1:1: Error: Variables must be' in str(CE.value)

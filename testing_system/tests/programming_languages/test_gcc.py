# encoding: utf-8
from pathlib import Path
import pytest

from worker.exceptions import CompilationError


@pytest.mark.parametrize('compiler_image', ('gcc', 'gcc11'))
def test_gcc_compiler_OK(compiler_image, compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.c')
    OK_solution_path.write(
            '#include "stdio.h"\n'
            'int main() {'
            '  FILE *fout = fopen("output.txt", "w");'
            '  fprintf(fout, "Hello World!");'
            '  return 0;'
            '}'
        )
    testing_report = compiler_runner(
            compiler_image,
            runner='binary',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']


@pytest.mark.parametrize('compiler_image', ('gcc', 'gcc11'))
def test_gcc_compiler_CE(compiler_image, compiler, tmpdir, hello_world_problem):
    CE_solution_path = tmpdir.join('CE_solution.c')
    CE_solution_path.write('if')
    with pytest.raises(CompilationError) as CE:
        compiler(
                compiler_image,
                problem_path=hello_world_problem['path'],
                solution_source_path=Path(CE_solution_path)
            )
    assert 'solution.source:1:1: error: expected identifier or \'(\' before \'if' in str(CE.value)

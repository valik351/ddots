# encoding: utf-8
from pathlib import Path
import pytest

from worker.exceptions import CompilationError


def test_go_compiler_OK(compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.go')
    OK_solution_path.write(
            'package main\n'
            'import "io/ioutil"\n'
            'func main() { ioutil.WriteFile("output.txt", []byte("Hello World!"), 0644) }'
        )
    testing_report = compiler_runner(
            'go',
            runner='binary',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']

def test_go_compiler_CE(compiler, tmpdir, hello_world_problem):
    CE_solution_path = tmpdir.join('CE_solution.go')
    CE_solution_path.write('if')
    with pytest.raises(CompilationError) as CE:
        compiler(
                'go',
                problem_path=hello_world_problem['path'],
                solution_source_path=Path(CE_solution_path)
            )
    assert 'solution.source:1:1: expected \'package\', found \'if\'' in str(CE.value)

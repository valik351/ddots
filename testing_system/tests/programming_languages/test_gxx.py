# encoding: utf-8
from pathlib import Path
import pytest

from worker.exceptions import CompilationError


@pytest.mark.parametrize('compiler_image', ('gxx', 'gxx11', 'gxx14', 'gxx17'))
def test_gxx_compiler_OK(compiler_image, compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.cpp')
    OK_solution_path.write(
            '#include "fstream"\n'
            'int main() {'
            '  std::ofstream fout("output.txt");'
            '  fout << "Hello World!";'
            '  return 0;'
            '}'
        )
    testing_report = compiler_runner(
            compiler_image,
            runner='binary',
            problem_path=hello_world_problem['path'],
            solution_source_path=Path(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']


@pytest.mark.parametrize('compiler_image', ('gxx', 'gxx11', 'gxx14', 'gxx17'))
def test_gxx_compiler_CE(compiler_image, compiler, tmpdir, hello_world_problem):
    CE_solution_path = tmpdir.join('CE_solution.cpp')
    CE_solution_path.write('if')
    with pytest.raises(CompilationError) as CE:
        compiler(
                compiler_image,
                problem_path=hello_world_problem['path'],
                solution_source_path=Path(CE_solution_path)
            )
    assert 'solution.source:1:1: error: expected unqualified-id before \'if\'' in str(CE.value)

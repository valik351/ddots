# encoding: utf-8
from unittest import mock

import asyncio
from requests import Response
import pytest

from worker import exceptions, extensions


@pytest.mark.asyncio
async def test_worker_test_solution_full(current_app, hello_world_problem):
    report = await current_app.test_solution(
            programming_language_id=current_app.config.DOTS_LANG_ID_FPC,
            solution_source=(
                b'begin'
                b'  Assign(output, \'output.txt\');'
                b'  Rewrite(output);'
                b'  WriteLn(\'Hello World!\');'
                b'end.'
            ),
            problem_path=hello_world_problem['path'],
            testing_mode='full'
        )

    assert isinstance(report, str)
    report_lines = report.strip().split('\n')
    assert len(report_lines) == 6
    assert report_lines[3] == ''

    for report_line_id, report_line in enumerate(report_lines[:3], 1):
        parsed_report_line = report_line.split(' ')
        assert len(parsed_report_line) == 5, report_line
        test_id, report_status, points, execution_time, memory_peak = parsed_report_line
        assert int(test_id) == report_line_id
        assert report_status == 'OK'
        assert float(points) > 0
        assert int(execution_time) >= 0
        assert int(memory_peak) >= 0


@pytest.mark.asyncio
async def test_worker_test_solution_first_fail(current_app, hello_world_problem):
    report = await current_app.test_solution(
            programming_language_id=current_app.config.DOTS_LANG_ID_FPC,
            solution_source=(
                b'begin'
                b'  Assign(output, \'output.txt\');'
                b'  Rewrite(output);'
                b'  WriteLn(\'Hello World!\');'
                b'end.'
            ),
            problem_path=hello_world_problem['path'],
            testing_mode='first-fail'
        )

    assert isinstance(report, str)
    report_lines = report.strip().split('\n')
    assert len(report_lines) == 6
    assert report_lines[3] == ''

    for report_line_id, report_line in enumerate(report_lines[:3], 1):
        parsed_report_line = report_line.split(' ')
        assert len(parsed_report_line) == 5, report_line
        test_id, report_status, points, execution_time, memory_peak = parsed_report_line
        assert int(test_id) == report_line_id
        assert report_status == 'OK'
        assert float(points) > 0
        assert int(execution_time) >= 0
        assert int(memory_peak) >= 0


@pytest.mark.asyncio
async def test_worker_test_solution_one(current_app, hello_world_problem):
    report = await current_app.test_solution(
            programming_language_id=current_app.config.DOTS_LANG_ID_FPC,
            solution_source=(
                b'begin'
                b'  Assign(output, \'output.txt\');'
                b'  Rewrite(output);'
                b'  WriteLn(\'Hello World!\');'
                b'end.'
            ),
            problem_path=hello_world_problem['path'],
            testing_mode='one'
        )

    assert isinstance(report, str)
    report_lines = report.strip().split('\n')
    assert len(report_lines) == 4
    assert report_lines[1] == ''

    for report_line_id, report_line in enumerate(report_lines[:1], 1):
        parsed_report_line = report_line.split(' ')
        assert len(parsed_report_line) == 5, report_line
        test_id, report_status, points, execution_time, memory_peak = parsed_report_line
        assert int(test_id) == report_line_id
        assert report_status == 'OK'
        assert float(points) > 0
        assert int(execution_time) >= 0
        assert int(memory_peak) >= 0


def test_worker_run_worker_loop(current_app):
    new_solution_id = '1_1000.1001.02F'
    report = '1 OK 100 0 0\n\n'

    def fake_dots_api_get(url, data=None):
        response = Response()
        if url == '/solution/':
            response.status_code = 200
            response._content = new_solution_id.encode('utf-8')
        elif url == '/solution/%s' % new_solution_id:
            response.status_code = 200
            response._content = b''
            current_app._break_event.set()
        elif url == '/lock/%s' % new_solution_id:
            response.status_code = 200
            response._content = b''
        elif url == '/result/%s' % new_solution_id:
            assert data == report.encode('utf-8')
            response.status_code = 200
            response._content = b'1 OK'
        return response

    def _mpo(*args, return_value_async=mock.DEFAULT, **kwargs):
        """
        mock patch object helper
        """
        async def mock_fn(*_args, **_kwargs):
            return return_value_async

        if return_value_async is not mock.DEFAULT:
            kwargs['new'] = mock.Mock(wraps=mock_fn)
        return mock.patch.object(*args, **kwargs)

    with \
            _mpo(extensions.dots_api, 'authenticate', return_value_async=None) as mocked_auth, \
            _mpo(extensions.dots_api, '_get_blocking', mock.Mock(wraps=fake_dots_api_get)) as mocked_get, \
            _mpo(extensions.problems_manager, 'prepare_problem', return_value_async='1000') as mocked_prepare_problem, \
            _mpo(current_app, 'test_solution', return_value_async=report) as mocked_test_solution:
        async def run_worker_loop():
            await current_app.run_worker_loop()
            current_task = asyncio.current_task()
            done, pending = await asyncio.wait([t for t in asyncio.all_tasks() if t != current_task], timeout=5)
            assert not pending
        asyncio.run(run_worker_loop())

    assert mocked_auth.call_count == 1
    assert mocked_get.call_count == 4
    assert mocked_prepare_problem.call_count == 1
    assert mocked_test_solution.call_count == 1

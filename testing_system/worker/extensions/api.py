# encoding: utf-8

import logging
import re
import time

import requests

from ..utils import log_response, force_async
from . import runners_manager


log = logging.getLogger(__name__)  # pylint: disable=invalid-name


class DOTSApi(object):

    MAX_RETRIES = 10
    RETRIES_DELAY = 2.0
    DEFAULT_HTTP_TIMEOUT = 30.0

    TESTING_MODE_CODE_TO_NAME = {
		'F': runners_manager.TM_FULL,
		'A': runners_manager.TM_FIRST_FAIL,
		'S': runners_manager.TM_ONE,
		'T': runners_manager.TM_ONE,
	}

    # Solution name example: 172484_5046.1001.03F
    SOLUTION_NAME_REGEX = re.compile(
        r'^(?P<solution_id_code>\d+)_' \
            r'(?P<problem_id>\d+)\.'
            r'(?P<user_id>\d+)\.'
            r'(?P<programming_language_id>\d+)(?P<testing_mode_code>[%s])$' % (
                ''.join(TESTING_MODE_CODE_TO_NAME.keys())
            )
    )

    def __init__(self, app=None):
        self.session = requests.Session()
        self.session.mount('', requests.adapters.HTTPAdapter(max_retries=self.MAX_RETRIES))
        self.access_token = None
        self.base_session_url = None

        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        self.base_url = app.config.DOTS_API_URL
        if not app.config.DOTS_API_USE_KEEP_ALIVE_CONNECTIONS:
            # KHCup DOTS setup has issues with keep-alive connections, so as a
            # workaround we ask server to close connections instead of keeping
            # them alive.
            self.session.headers['Connection'] = 'close'
        self.__supported_programming_language_ids = set(app.config.DOTS_LANG_ID_TO_RUNNER.keys())

    def _get_blocking(self, url_suffix, timeout=DEFAULT_HTTP_TIMEOUT, **kwargs):
        for retries_left in range(self.MAX_RETRIES, 0, -1):
            try:
                return self.session.get(
                    self.base_session_url + url_suffix,
                    timeout=timeout,
                    **kwargs
                )
            except requests.ConnectionError as exc:
                if retries_left == 1:
                    raise exc
            time.sleep(self.RETRIES_DELAY)

    @force_async
    def _get(self, *args, **kwargs):
        return self._get_blocking(*args, **kwargs)

    @force_async
    def authenticate(self, username, password):
        """
        Get DOTS Session ID
        """
        self.session.auth = (username, password)
        auth_response = self.session.get(
            '%s/bot/' % self.base_url,
            timeout=self.DEFAULT_HTTP_TIMEOUT
        )

        if auth_response.status_code != 200:
            log_response(log.error, "Cannot get a session ID.", auth_response)
            # Raise proper ConnectionError or HTTPError
            auth_response.raise_for_status()

        if 'DSID' not in auth_response.cookies:
            log.error("DSID is not set in the session response")
            raise requests.ConnectionError("DSID is not set")

        self.access_token = auth_response.cookies['DSID']
        log.debug("Session ID is %s", self.access_token)

        self.base_session_url = '%s/sess/%s/bot' % (self.base_url, self.access_token)

    async def try_pull_new_solution(self):
        new_solution_response = await self._get('/solution/')
        log_response(log.debug, "SOLUTION RESPONSE", new_solution_response)
        if new_solution_response.status_code != 200:
            log_response(
                    log.warning,
                    "Something went wrong while checking for new solutions.",
                    new_solution_response
                )
            return

        solution_id = new_solution_response.text
        if solution_id == '0 no solutions':
            log.debug("No solutions")
            return

        log.info("New solution '%s' has arrived", solution_id)

        try:
            solution_info = self.SOLUTION_NAME_REGEX.search(solution_id).groupdict()
        except AttributeError:
            log.warning("Solution name '%s' has wrong format", solution_id)
            return

        programming_language_id = solution_info['programming_language_id']
        if programming_language_id not in self.__supported_programming_language_ids:
            log.info(
                    "Programming language id '%s' used in the solution '%s' is not supported",
                    programming_language_id,
                    solution_id
                )
            return

        solution_info.update({
            'solution_id': solution_id,
            'testing_mode': self.TESTING_MODE_CODE_TO_NAME[solution_info['testing_mode_code']],
        })
        return solution_info

    async def get_solution_source(self, solution_id):
        solution_source_response = await self._get('/solution/%s' % solution_id)
        log_response(log.debug, "SOLUTION SOURCE RESPONSE", solution_source_response)
        if solution_source_response.status_code != 200:
            log_response(
                    log.error,
                    "Something went wrong while receiving the solution '%s'.",
                    solution_source_response,
                    solution_id
                )
            return
        log.info("The source code of the solution '%s' has been received", solution_id)
        return solution_source_response.content

    async def lock_solution(self, solution_id):
        solution_lock_response = await self._get('/lock/%s' % solution_id)
        log_response(log.debug, "SOLUTION LOCK RESPONSE", solution_lock_response)
        if solution_lock_response.status_code != 200:
            log_response(
                    log.error,
                    "Something went wrong while locking the solution '%s'.",
                    solution_lock_response,
                    solution_id
                )
        log.info("Solution '%s' has been locked", solution_id)
        return solution_lock_response

    async def unlock_solution(self, solution_id):
        solution_unlock_response = await self._get('/unlock/%s' % solution_id)
        if solution_unlock_response.status_code != 200:
            log_response(
                    log.warning,
                    "Unlocking the solution '%s' has failed.",
                    solution_unlock_response,
                    solution_id
                )
        else:
            log.info("Solution '%s' has been successfully unlocked.", solution_id)
        return solution_unlock_response

    async def send_report(self, solution_id, report):
        report_data = report.encode('utf-8')
        log.debug(
                "Sending report to '/result/%s': %r",
                solution_id,
                report_data
            )
        send_report_response = await self._get('/result/%s' % solution_id, data=report_data)
        if send_report_response.status_code != 200:
            log_response(
                    log.error,
                    "Posting the result testing report for the solution '%s' has failed.",
                    send_report_response,
                    solution_id
                )
            return
        if send_report_response.text != '1 OK':
            log_response(
                    log.error,
                    "The testing report for the solution '%s' has been declined.",
                    send_report_response,
                    solution_id
                )
            return
        log.info("The testing report for solution '%s' has been sent", solution_id)
        return send_report_response

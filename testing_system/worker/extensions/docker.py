# encoding: utf-8

import asyncio
import logging
import time

import docker
import requests

from ..exceptions import DockerTimeout, DockerError, ContainerTimeout


log = logging.getLogger(__name__)  # pylint: disable=invalid-name


class DockerManager(object):

    DOCKER_IMAGE_PREFIX = 'ddots-'

    DEFAULT_DOCKER_TIMEOUT = 60
    DEFAULT_CONTAINER_TIMEOUT = 20

    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        self._docker_client = docker.DockerClient(base_url=app.config.DOCKER_DAEMON_API_URL)
        self._memory_hard_limit = app.config.CONTAINER_MEMORY_HARD_LIMIT
        self._shared_volumes = app.config.SHARED_VOLUMES

    def _get_image_name(self, container_suffix):
        return self.DOCKER_IMAGE_PREFIX + container_suffix

    def _get_container_name(self, container_suffix):
        raise NotImplementedError()

    def _get_container_init_options(self, container_suffix):
        return {
            'image': self._get_image_name(container_suffix),
            'name': self._get_container_name(container_suffix),
            'network_disabled': True,
            'environment': {
                    'TMPDIR': str(self._shared_volumes['sandbox']['mount_point']),
                },
            'volumes': [
                    str(self._shared_volumes['data']['mount_point']),
                    str(self._shared_volumes['sandbox']['mount_point']),
                ],
            'host_config': self._docker_client.api.create_host_config(
                    binds={
                            self._shared_volumes['data']['volume_name']: {
                                    'bind': str(self._shared_volumes['data']['mount_point']),
                                    'mode': 'ro',
                                },
                            self._shared_volumes['sandbox']['volume_name']: {
                                    'bind': str(self._shared_volumes['sandbox']['mount_point']),
                                    'mode': 'rw',
                                },
                        },
                    read_only=True,
                    mem_limit=self._memory_hard_limit,
                    memswap_limit=self._memory_hard_limit,
                ),
        }

    def kill_container(self, container_suffix):
        container_name = self._get_container_name(container_suffix)

        while 1:
            try:
                self._docker_client.api.kill(container_name)
            except docker.errors.APIError:
                pass
            except requests.exceptions.ReadTimeout:
                log.error("Killing '%s' container has timed out.", container_name)
                raise DockerTimeout("kill_container has timed out.")

            try:
                if not self._docker_client.api.inspect_container(container_name)['State']['Running']:
                    break
            except requests.exceptions.ReadTimeout:
                log.warning(
                    "Inspecting '%s' container has timed out while trying to kill it.",
                    container_name
                )

            log.warning(
                "Killing '%s' container has failed, container is still running. "
                "Retrying in %d seconds...",
                container_name,
                self.DEFAULT_DOCKER_TIMEOUT
            )
            time.sleep(self.DEFAULT_DOCKER_TIMEOUT)

        log.info("Container '%s' has been killed.", container_name)

    def rm_container(self, container_suffix):
        container_name = self._get_container_name(container_suffix)
        try:
            self._docker_client.api.remove_container(container_name)
        except docker.errors.NotFound:
            log.debug(
                "Container '%s' has not been removed because it did not exist.",
                container_name
            )
        except requests.exceptions.ReadTimeout:
            log.warning("Removing '%s' container has timed out.", container_name)
        else:
            log.debug("Container '%s' has been removed.", container_name)

    def init_containers_list(self, containers_suffix_list):
        async def _init():
            async_pool = asyncio.get_event_loop()
            await asyncio.gather(*[
                    async_pool.run_in_executor(None, self.init_container, container_suffix) \
                        for container_suffix in containers_suffix_list
                ])
        asyncio.run(_init())

    def init_container(self, container_suffix):
        container_name = self._get_container_name(container_suffix)
        log.debug("Initializing '%s' container...", container_name)

        # Cleanup before we start
        self.rm_container(container_suffix)

        try:
            # Create a container via running it with all necessary options so
            # we can start it later.
            init_options = self._get_container_init_options(container_suffix)
            log.debug("Container '%s' initialization options: %s", container_name, init_options)
            container_reference = self._docker_client.api.create_container(**init_options)
            container_info = self._docker_client.api.inspect_container(container_reference)
        except requests.exceptions.ReadTimeout:
            container_info = {
                    'State': {
                            'Status': "Init process timed out",
                        },
                }

        if container_info['State']['Status'] != 'created':
            # Our docker containers return exit code == 1 and report an error
            # if no input file was found, but this is perfectly fine for the
            # initialization process!
            raise DockerError(
                    "Container '%s' has failed to init. Its current state is %r" % (
                        container_name,
                        container_info['State']
                    )
                )

        log.info("Container '%s' has been initialized.", container_name)

    def start(self, container_suffix, timeout=None):
        """
        Returns:
            (exit_code, (stdin, stderr))
        """
        if timeout is None:
            timeout = self.DEFAULT_CONTAINER_TIMEOUT

        container_name = self._get_container_name(container_suffix)

        container_outputs_size = [0, 0]
        container_outputs = ([], [])

        try:
            container_outputs_stream = self._docker_client.api.attach(
                    container_name,
                    stream=True,
                    demux=True
                )

            self._docker_client.api.start(container_name)

            for outputs in container_outputs_stream:
                for output_type, output in enumerate(outputs):
                    if output is None:
                        continue
                    if len(output) > 100000:
                        output = output[:100000]
                    if container_outputs_size[output_type] >= 100000:
                        continue
                    container_outputs_size[output_type] += len(output)
                    container_outputs[output_type].append(output.decode('utf-8', errors='replace'))

            container_info = self._docker_client.api.inspect_container(container_name)
        except requests.exceptions.ReadTimeout:
            self.kill_container(container_suffix)
            raise ContainerTimeout()

        return (
                container_info['State']['ExitCode'],
                (
                    ''.join(container_outputs[0]),
                    ''.join(container_outputs[1]),
                )
            )

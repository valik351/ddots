# encoding: utf-8

import asyncio
import logging
import os
import tempfile
import threading
import time

import requests

from .exceptions import TestingSystemRestartSignal, TestingSystemError, CompilationError
from .extensions import dots_api, compilers_manager, problems_manager, runners_manager
from .utils import force_async


log = logging.getLogger(__name__)  # pylint: disable=invalid-name


class Config(object):
    # pylint: disable=invalid-name

    def __init__(self):
        self.DOTS_LANG_ID_TO_COMPILER = {}
        self.DOTS_LANG_ID_TO_RUNNER = {}

    def from_object(self, obj):
        for config_key in dir(obj):
            if config_key.isupper():
                setattr(self, config_key, getattr(obj, config_key))
        self.is_valid()

    def is_valid(self):
        if getattr(self, 'DOTS_USERNAME') is None:
            raise Exception("DOTS_USERNAME is a required setting.")
        if getattr(self, 'DOTS_PASSWORD') is None:
            raise Exception("DOTS_PASSWORD is a required setting.")
        assert \
            self.PREFETCH_SOLUTIONS_COUNT >= 2, \
            "Prefetching logic doesn't support prefetching less than 2 solutions"
        return True

    @property
    def AVAILABLE_COMPILERS(self):
        return set(self.DOTS_LANG_ID_TO_COMPILER.values())

    @property
    def AVAILABLE_RUNNERS(self):
        return set(self.DOTS_LANG_ID_TO_RUNNER.values())


class Worker(object):

    def __init__(self):
        self.config = Config()
        self._break_event = threading.Event()

    def _compile_solution(self, compiler_name, problem_path, solution_source_filename):
        log.info("Compiler '%s' is compiling the solution...", compiler_name)
        try:
            runnable_solution_filepath = compilers_manager.start(
                    compiler_name,
                    problem_path,
                    solution_source_filename
                )
        except TestingSystemError:
            log.exception("Testing System failed unexpectantly while compiling")
            raise
        log.info("The solution has been compiled")
        return runnable_solution_filepath

    @force_async
    def test_solution(self, programming_language_id, solution_source, problem_path, testing_mode):
        """
        Compile a solution if needed and run it against the problem tests.

        Args:
            programming_language_id (str): a unique programming language ID
            solution_source (str): a solution source text
            problem_path (str): a path to the problem files (Problem.xml,
                tests, etc)
            testing_mode (str): choice of ``full``, ``first-fail``, or ``one``
                (see also ``RunnersManager.TESTING_MODES``).

        Returns:
            report (str): a testing report in a custom space-separated format.
        """
        try:
            os.makedirs(self.config.SOLUTIONS_TEMP_ROOT)
        except FileExistsError:
            pass

        with tempfile.NamedTemporaryFile(
                    buffering=0,
                    dir=self.config.SOLUTIONS_TEMP_ROOT
                ) as tmp_solution_source_file:
            log.debug("The solution is being saved to disk")
            tmp_solution_source_file.write(solution_source)

            try:
                if programming_language_id in self.config.DOTS_LANG_ID_TO_COMPILER:
                    runnable_solution_filepath = self._compile_solution(
                            self.config.DOTS_LANG_ID_TO_COMPILER[programming_language_id],
                            problem_path,
                            tmp_solution_source_file.name
                        )
                else:
                    runnable_solution_filepath = tmp_solution_source_file.name

                log.info("The solution is being tested...")
                report = runners_manager.start(
                        self.config.DOTS_LANG_ID_TO_RUNNER[programming_language_id],
                        problem_path,
                        testing_mode,
                        runnable_solution_filepath,
                    )
            except CompilationError as exc:
                report = runners_manager.truncate_report(
                        '1 CE 0 0 0\n\n%s' % exc
                    )
            except Exception:
                log.exception("Testing System failed unexpectantly")
                raise TestingSystemRestartSignal()

        report += '\n--\nDdotS version: 1.0.0'
        log.debug("Testing report is:\n    %s", '\n    '.join(report.split('\n')))
        log.info("The solution has been tested")
        return report

    async def run_worker_loop(self):
        """
        The entry-point and the main process of DDOTS.
        """
        log.info("Processor loop is starting...")

        log.debug("Getting a session ID...")
        try:
            await dots_api.authenticate(
                    username=self.config.DOTS_USERNAME,
                    password=self.config.DOTS_PASSWORD,
                )
        except requests.ConnectionError:
            raise TestingSystemRestartSignal()

        log.info("Worker loop is waiting for new solutions at '%s'...", dots_api.base_url)

        while not self._break_event.is_set():
            solution_info = await dots_api.try_pull_new_solution()
            if solution_info is None:
                await asyncio.sleep(self.config.PREFETCH_DELAY_BETWEEN_CHECKS)
                await asyncio.wait(asyncio.all_tasks(), timeout=0)
                continue

            solution_id = solution_info['solution_id']

            problem_path = asyncio.create_task(
                    problems_manager.prepare_problem(solution_info['problem_id'])
                )

            solution_source = asyncio.create_task(dots_api.get_solution_source(solution_id))

            try:
                solution_source, problem_path = await asyncio.gather(solution_source, problem_path)
            except TestingSystemError:
                asyncio.create_task(dots_api.unlock_solution(solution_id))
                continue

            if solution_source is None:
                continue

            solution_lock_response = asyncio.create_task(dots_api.lock_solution(solution_id))

            try:
                report = await self.test_solution(
                        solution_info['programming_language_id'],
                        solution_source,
                        problem_path,
                        solution_info['testing_mode']
                    )
            except TestingSystemError:
                asyncio.create_task(dots_api.unlock_solution(solution_id))
                continue

            if (await solution_lock_response).status_code != 200:
                continue

            asyncio.create_task(dots_api.send_report(solution_id, report))

        log.info("Worker loop is shutting down...")

    def run(self):
        """
        This is the worker supervisor.
        """
        while not self._break_event.is_set():
            try:
                asyncio.run(self.run_worker_loop())
            except TestingSystemRestartSignal:
                if self._break_event.is_set():
                    log.info("Soft shutdown")
                    break
                log.exception("Soft restart")
            except requests.exceptions.ConnectionError as exception:
                log.warning("Connection Error: %s", exception)
            except Exception:  # pylint: disable=broad-except
                log.exception("Something went wrong. Restarting in 10 seconds...")
                time.sleep(9)
            time.sleep(1)

    def stop(self):
        log.warning("Stopping the worker...")
        self._break_event.set()
